class scm-nginx {

  include java

  exec { 'scm repository':
    command => 'echo "deb http://maven.scm-manager.org/nexus/content/repositories/releases ./" >> /etc/apt/sources.list',
    path    => ["/bin/","/sbin/","/usr/bin/","/usr/sbin/"];
  }

  exec { 'scm repository key':
    command => 'apt-key adv --recv-keys --keyserver keyserver.ubuntu.com D742B261',
    path    => ["/bin/","/sbin/","/usr/bin/","/usr/sbin/"];
  }

  exec { 'apt-get update':
    command => 'apt-get update',
    path    => ["/bin/","/sbin/","/usr/bin/","/usr/sbin/"],
    require => [Exec['scm repository'], Exec['scm repository key']];
  }

  $sysPackages = [ "mercurial", "scm-server", "nginx" ]
  package { $sysPackages:
    ensure  => "installed",
    require => Exec['apt-get update'];
  }

  file { '/etc/nginx/sites-available/default':
    ensure  => present,
    source  => "/vagrant/puppet/files/reverseproxy.conf",
    require => Package["nginx"];
  }

  file { '/opt/scm-server/conf/server-config.xml':
    ensure  => present,
    source  => "/vagrant/puppet/files/server-config.xml",
    require => [Package["scm-server"],Class['java']];
  }

  service { "nginx":
    enable  => true,
    ensure  => running,
    require => File["/etc/nginx/sites-available/default"];
  }

  service { "scm-server":
    enable  => true,
    ensure  => running,
    require => File['/opt/scm-server/conf/server-config.xml'];
  }

}

include scm-nginx
